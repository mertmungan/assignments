// BASE EXPORT
import React, { useState, useEffect } from 'react'

// SERVICES
import axios from 'axios'


//3rd Party
import { useHistory } from 'react-router-dom'
import Grid from '@mui/material/Grid'



import '../App.css'


function CharacterDetail() {
  const history = useHistory() //to handle redirecting
  const [rmCharDetails, setCharDetails] = useState(null) //holds the characters to display
  const [episodes, setEpisodes] = useState([]) //hold the episodes to display


  const GetCharacterDetails = async () => { //function to get character details
    const url = `https://rickandmortyapi.com/api/character/${
      window.location.href.split('/')[3].split('character')[1] //splitting the url to get character's number
    }`
    const response = await axios.get(url)
    setCharDetails(response.data) //set CharDetails
  }

  const GetEpisodes = async (charDetailsItem) => { //function to get character's last 5 episodes
    if (charDetailsItem?.episode) { //check if charDetails is empty or not
      const lastFiveEpisodes = charDetailsItem?.episode?.slice(-5) //if charDetails has episodes then get the last 5 episodes
      const episodeNumbers = [] //array to store episode numbers
      const episodeNames = [] //array to store episode names
      for (let i = 0; i <= 4; i++) {
        episodeNumbers.push(lastFiveEpisodes[i]?.split('/')[5]) //splitting the url to get the episode numbers
        const url2 = `https://rickandmortyapi.com/api/episode/${episodeNumbers[i]}`
        const response2 = await axios.get(url2)
        episodeNames.push(response2.data.name) //push episode names to episodeNames array
      }
      setEpisodes(episodeNames) //set episode names
    }
  }

  useEffect(() => {
    GetCharacterDetails() //get character details from the API
  }, [])

  useEffect(() => {
    GetEpisodes(rmCharDetails) //get the last 5 episodes for the character
  }, [rmCharDetails]) // then update it

  function handleClick() { 
    history.push('/') //redirect the user to the main page
  }

  return (
    <>
          <Grid
        container
        columns={{ xs: 4, sm: 8, md: 12 }}
>
         <div className="detail-page-container">        
         <Grid item xs={2} sm={2} md={2} >
         <button onClick={handleClick} className="back-button">JEEZ TAKE ME BACK</button>
         </Grid>
        {rmCharDetails && (
          <Grid item xs={2} sm={2} md={2} >
          <div className="character-details-container">
            <img src={rmCharDetails.image} alt='Character' loading="lazy"/>
            <h2> CHARACTER NAME </h2>
            <span className="character-name">{rmCharDetails.name}</span>
            <h2> DIMENSION </h2>
            <span className="character-origin">{rmCharDetails.origin.name}</span>
            <h2> LAST 5 EPISODES </h2>
            <span reversed className="episode-list">
              <li className="episode">{episodes[4]}</li>
              <li className="episode">{episodes[3]}</li>
              <li className="episode">{episodes[2]}</li>
              <li className="episode">{episodes[1]}</li>
              <li className="episode">{episodes[0]}</li>
            </span>
          </div>
          </Grid>
        )}
        </div>
        </Grid>
    </>
  )
}

export default CharacterDetail
