// BASE EXPORT
import React, { useEffect, useState, useCallback, useRef } from 'react'
// PAGES
import CharacterDetail from './pages/CharacterDetail'
// UI
import Item from './components/UI/Item'
// SERVICES
import axios from 'axios'
// 3rd Party
import { BrowserRouter as Router, Route, useHistory } from 'react-router-dom'
import Grid from '@mui/material/Grid'

// OTHERS
import { waitForSeconds } from './utils/helpers'
import './App.css'

// PAGE CONSTANTS
const TOTAL_PAGES = 35
const rmUrlWithPageParams = 'https://rickandmortyapi.com/api/character/?page='

//LOADER TO DISPLAY DURING THE API CALLS
const Loader = () => {
  return <div className='loader-div'>...</div>
}

function Scrollable() {
  //STATES FOR HANDLING THE SCROLLING EVENT
  const [rmCharItems, setRMCharItems] = useState([]) //holds the data items to display
  const [isLoading, setIsLoading] = useState(false) //loader will be displayed based on this state
  const [hasMore, setHasMore] = useState(true) //to check if there is more data to fetch or not
  const [pages, setPages] = useState(1) //current number of pages to request API call
  const observer = useRef() //to use in the Mozilla's Intersection Observer API

  useEffect(() => {
    getItems(pages) //fetch the data from the API
    setPages((pages) => pages + 1) //increase the number of the current page
  }, [])

  //FUNCTION TO CHECK IF THE ITEM IS THE LAST ONE IN THE LIST OR NOT
  const lastItemRef = useCallback(
    (node) => {
      //dummy variable for item
      if (isLoading) return
      if (observer.current) observer.current.disconnect() //stop watching the targeted element for visibility changes

      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          //check if the targeted element intersects with the intersection observer's root and if there are more items to be fetched
          if (pages < TOTAL_PAGES) {
            getItems(pages) //fetch the data from the API
            setPages((pages) => pages + 1) //increase the number of the current page
          } else {
            setHasMore(false) //no more pages so set hasMore false
          }
        }
      })

      if (node) observer.current.observe(node) //if there is a node then add that node to the set of target nodes being watched by the IntersectionObserver
    },
    [isLoading, hasMore]
  )

  const getItems = async (page) => {
    setIsLoading(true) //make loader visible during API calls
    waitForSeconds(1) //waiting time to make the loader visible
    await axios.get(rmUrlWithPageParams + page).then((resp) => {
      //get the characters for the current page
      setRMCharItems([...rmCharItems, ...resp.data.results]) //then set them
      setIsLoading(false) //then make the loader invisible
    })
  }

  let history = useHistory() //to handle redirecting

  function handleClick(index) {
    history.push(`/character${index + 1}`) //redirect the user to character details page
  }

  return (
    <div className="page-container">
        <div className="header">
          <span className="title">
            JEEZ RICK WHERE ARE WE?
            </span>
            </div>
      <Grid
        container
        direction='row'
        justifyContent='center'
        alignItems='center'
        spacing={{ xs: 2, md: 3 }}
      >
        {rmCharItems.map((item, index) =>
          index + 1 === rmCharItems.length ? ( //check if it is the last element to map or not
            //IF IT IS THE LAST ITEM
            <Grid
              style={{ marginBottom: 5 }} gutterBottom
              xs={3}
            >
              <Item reference={lastItemRef} onClick={() => handleClick(index)}>
                <button className="character-div">
                  <img
                    src={item.image}
                    alt={item.name}
                    className="character-image"
                    loading="lazy"
                  />
                  <div className="character-details">
                  <h2 className="character-name"> {item.name} </h2> <br/>
                    {item.status} <br />
                    {item.species} <br />
                    {item.type}
                  </div>
                </button>
              </Item>
            </Grid>
          ) : (
            // IF IT IS NOT THE LAST ITEM
            <Grid style={{ marginBottom: 5}} key={index} xs={3} gutterBottom>
              <Item key={index}>
                <button className='character-div' onClick={() => handleClick(index)}>
                  <img
                    src={item.image}
                    className='character-image'
                  />
                  <div className='character-details'>
                    <h2> {item.name} </h2>
                    {item.status} <br />
                    {item.species} <br />
                    {item.type}
                  </div>
                </button>
              </Item>
            </Grid>
          )
        )}
        {
          /*IF IS LOADING IS TRUE THEN DISPLAY THE LOADER*/ isLoading && (
            <Loader />
          )
        }
      </Grid>
    </div>
  )
}

function App() {
  return (
    <>
      <Router>
        <Route path='/' exact component={Scrollable} />
        <Route path='/character:id' exact component={CharacterDetail} />
        {/* 400 Route */}
      </Router>
    </>
  )
}

export default App
