import React from 'react'

function Item(props) {
  const { children, reference } = props
  return <div ref={reference}>{children}</div>
}

export default Item
