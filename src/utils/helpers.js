export const waitForSeconds = (ms) => { //function to wait for the loader
  return new Promise((resolve) => setTimeout(resolve, ms * 1000))
}
